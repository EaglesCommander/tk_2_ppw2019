# PPW-TK2
[![pipeline](https://gitlab.com/EaglesCommander/tk_2_ppw2019/badges/master/pipeline.svg)](http://electronic-finder-2.herokuapp.com)
[![coverage report](https://gitlab.com/EaglesCommander/tk_2_ppw2019/badges/master/coverage.svg)](https://gitlab.com/EaglesCommander/tk_2_ppw2019/commits/master)

Tugas Kelompok 2 PPW 2019

Kelompok: KB13

Link herokuapp: http://electronic-finder-2.herokuapp.com

## Anggota
1. Muhammad Salman Al-farisi (1806133843)
2. Shella Gabriella (1806205350)
3. Nadhif Adyatma Prayoga (1806205501)
4. Muhammad Ryan Anshar Haryanto (1806147073)

## Nama Aplikasi
E-Fi - Electronics Finder

## Deskripsi Website
Apakah anda pernah merasa penasaran apa brand handphone yang teman anda miliki? Atau spesifikasinya? Dimana membelinya? Atau mungkin anda ingin membeli keyboard yang sama dengan yang anda sudah punya untuk sekian lama tahun? E-Fi adalah sebuah website yang dapat mencari suatu barang elektronik dengan sebuah foto! Anda hanya perlu mengambil gambar barang elektronik yang ingin cari dan upload ke website kami! Atau mungkin anda ingin hanya mencari barang yang sudah anda ketahui, anda juga dapat mencarinya di website kami. Kami akan menampilkan hasil yang paling cocok dengan pencarian anda secara detail. Anda juga dapat mencari toko yang menjual barang yang anda cari dan membookingnya! Anda hanya perlu memberikan email dan nomor telfon anda dan selesai!

E-Fi atau Electronic Finder adalah sebuah website dimana anda bisa mencari sebuah barang elektronik dengan gambar maupun teks. Hasil paling cocok akan ditampilkan oleh website dan user dapat melihat detail barang tersebut. Lalu user dapat melihat dimana barang tersebut dijual, detail tokonya mulai dari nama, alamat, sampai stok tersisa juga ditampilkan. Lalu user dapat membooking item dengan mengisi sebuah form, dimana pemilik toko akan diberi notifikasi atas booking tersebut.

## Daftar Fitur
Berikut fitur-fitur yang kami buat :
1. Homepage, page ini adalah page pertama yang dikunjungi oleh user. Di page ini ada dua buah form yang tersedia, satu untuk mengupload image yang ingin dicari dan satu lagi untuk mencari dengan input text. Apapun form yang diisi, tombol lanjut akan mengantarkan ke halaman 2.
2. Search result, page ini adalah page yang tampil setelah user mengisi form di halaman 1. Page ini berisi detail tentang barang yang dicari oleh user, dari segi spesifikasi sampai manufucturer. Page ini juga berisi sebuah button yang dapat di click untuk menampilkan dimana user dapat membeli barang tersebut. Di bagian paling bawah juga ada sebuah form untuk user menginput emailnya untuk menjadi member newsletter website.
3. Store result, page ini adalah page yang tampil setelah user mengclick button di halaman 2 untuk mencari dimana bisa mendapatkan barang tersebut. Page ini berisi list store dimana barang pada halaman 2 dapat dibeli. Di masing - masing entry store ada detail nama, lokasi, dan info lainnya seperti stock barang tersisa, dan satu tombol untuk booking barang tersebut. Sama seperti halaman 2, di bagian paling bawah juga ada sebuah form untuk user menginput emailnya untuk menjadi member newsletter website.
4. Booking form, page ini adalah page yang tampil setelah user mengclick button untuk booking pada halaman 3. Page ini berisi sebuah form berisi nama, email, no. telfon, dan pesan. Dimana setelah tombol ditekan, form tersebut akan disimpan ke database dan copy dari form tersebut akan di email ke user.

Keterangan tambahan : Feature Homepage dan Booking berada di app yang sama yaitu app market.