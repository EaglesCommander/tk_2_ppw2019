from django.test import TestCase
from django.test import Client
from django.urls import resolve

import json

from .views import searchpage, subscribe, items_stored
from .models import Electronic, Subscribtion
from django.http import HttpRequest
import unittest

from io import BytesIO
from PIL import Image
from django.core.files.base import File

def get_image_file(name='test.png', ext='png', size=(50, 50), color=(256, 0, 0)):
    file_obj = BytesIO()
    image = Image.new("RGBA", size=size, color=color)
    image.save(file_obj, ext)
    file_obj.seek(0)
    return File(file_obj, name=name)

class SearchPageTest(TestCase):
	def test_storepage_exist(self):
		pure = Electronic()
		pure.item_id = 1
		pure.brand = "sumsang"
		pure.picture = get_image_file()
		pure.name = "10S"
		pure.processor = "amd"
		pure.display = "tn panel"
		pure.graphic = "amd gpu"
		pure.memory = "90gb"
		pure.camera = "12mp"
		pure.battery = "4000 mah"
		pure.save()
		response = Client().get('/searchresult/')
		self.assertEqual(response.status_code,200)
		response = Client().post('/searchresult/',{'item_id':1})
		self.assertEqual(response.status_code, 200)

	def test_using_storepage_func(self):
		found = resolve('/searchresult/')
		self.assertEqual(found.func, searchpage)

	def test_subscribtion(self):
		response = Client().get('/searchresult/subscribe/')
		self.assertEqual(response.status_code,302)

	def test_using_subscribe_func(self):
		found = resolve('/searchresult/subscribe/')
		self.assertEqual(found.func, subscribe)

	def test_items_stored_exist(self):
		response = Client().get('/searchresult/get_item/')
		self.assertEqual(response.status_code, 200)

	def test_using_items_stored_func(self):
		found = resolve('/searchresult/get_item/')
		self.assertEqual(found.func, items_stored)

	def test_model_exist(self):
		pure = Electronic()
		pure.item_id = 1
		pure.brand = "sumsang"
		pure.picture = get_image_file()
		pure.name = "10S"
		pure.processor = "amd"
		pure.display = "tn panel"
		pure.graphic = "amd gpu"
		pure.memory = "90gb"
		pure.camera = "12mp"
		pure.battery = "4000 mah"
		pure.save()

		self.assertEqual(Electronic.objects.count(), 1)
		self.assertIsInstance(pure, Electronic)
		subs = Subscribtion()
		subs.email = "Something@gmail.com"
		subs.save()

		self.assertEqual(Subscribtion.objects.count(), 1)
		self.assertIsInstance(subs, Subscribtion)

	def test_subscription_form(self):
		client = Client()
		response = client.post("/searchresult/subscribe/", {'email':'monorail@gmail.com'})
		self.assertEqual(Subscribtion.objects.count(), 1)