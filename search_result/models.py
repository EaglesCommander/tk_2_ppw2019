from django.db import models

# Create your models here.
class Subscribtion(models.Model):
    email = models.EmailField(("Email"), max_length=254)
    class Meta:
        db_table = u'myemail_list'


class Electronic(models.Model):
	item_id = models.IntegerField(null = True)
	brand = models.CharField(max_length = 15)
	picture = models.ImageField(upload_to="store_img", null = True)
	name = models.CharField(max_length = 15)
	processor = models.CharField(max_length = 50)
	display = models.CharField(max_length = 50)
	graphic = models.CharField(max_length = 50)
	memory = models.CharField(max_length = 50)
	camera = models.CharField(max_length = 50)
	battery	= models.CharField(max_length = 50)
	class Meta:
		db_table = u'electronic_list'