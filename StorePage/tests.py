from django.test import TestCase
from django.test import Client
from django.urls import resolve

from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm

import json

from .views import storepage, subscribe, store_service
from .models import Store, Newsletter_Member
from django.http import HttpRequest

import unittest

from io import BytesIO
from PIL import Image
from django.core.files.base import File

def get_image_file(name='test.png', ext='png', size=(50, 50), color=(256, 0, 0)):
    file_obj = BytesIO()
    image = Image.new("RGBA", size=size, color=color)
    image.save(file_obj, ext)
    file_obj.seek(0)
    return File(file_obj, name=name)

class StorePageUnitTest(TestCase):

    def test_storepage_exist(self):
        response = Client().get('/storepage/')
        self.assertEqual(response.status_code,200)
        response = Client().post('/storepage/',{'item_id':1})
        self.assertEqual(response.status_code, 200)

    def test_using_storepage_func(self):
        found = resolve('/storepage/')
        self.assertEqual(found.func, storepage)

    def test_subscribe_exist(self):
        response = Client().get('/storepage/subscribe/')
        self.assertEqual(response.status_code, 302)
        
    def test_using_subscribe_func(self):
        found = resolve('/storepage/subscribe/')
        self.assertEqual(found.func, subscribe)

    def test_store_exist(self):
        response = Client().get('/storepage/get_stores/')
        self.assertEqual(response.status_code, 200)
    
    def test_using_store_func(self):
        found = resolve('/storepage/get_stores/')
        self.assertEqual(found.func, store_service)

    def test_login(self):
        user = User.objects.create_user('Nico', 'NicoNii@gmail.com', '252521')
        user.set_password('252521')
        user.save()

        client = Client()
        response = client.get('/storepage/')

        self.assertIn('login', response.content.decode('utf8'))
        
        logged_in = client.login(username='Nico', password='252521')
        self.assertTrue(logged_in)

        response = client.get('/storepage/')
        self.assertIn('Nico', response.content.decode('utf8'))

    def test_logout(self):
        user = User.objects.create_user('Nico', 'NicoNii@gmail.com', '252521')
        user.set_password('252521')
        user.save()

        client = Client()
        client.login(username='Nico', password='252521')

        response = client.get('/storepage/')
        self.assertIn('Nico', response.content.decode('utf8'))

        client.logout()

        response = client.get('/storepage/')
        self.assertIn('login', response.content.decode('utf8'))

    def test_models_works(self):
        mock_store = Store()
        mock_store.store_id = 1
        mock_store.picture = get_image_file()
        mock_store.name = "mock_store"
        mock_store.address_first = "mock_store_address1"
        mock_store.address_second = "mock_store_address2"
        mock_store.details = "mock_store_details"
        mock_store.save()

        self.assertEqual(Store.objects.count(), 1)
        self.assertIsInstance(mock_store, Store)

        mock_member = Newsletter_Member()
        mock_member.email = "Something@gmail.com"
        mock_member.save()

        self.assertEqual(Newsletter_Member.objects.count(), 1)
        self.assertIsInstance(mock_member, Newsletter_Member)

    def test_method_get(self):
        client = Client()
        response = client.get('/storepage/')

        self.assertIn("You haven't picked an item yet!", response.content.decode('utf8'))

    def test_method_post(self):
        client = Client()
        response = client.post('/storepage/', {'item_id':1})

        self.assertIn("You can buy", response.content.decode('utf8'))
    
    def test_newsletter_form_works(self):
        client = Client()
        response = client.post("/storepage/subscribe/", {'email':'nadhif.ap@gmail.com'})

        self.assertEqual(Newsletter_Member.objects.count(), 1)

