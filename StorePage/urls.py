from django.urls import path

from . import views

urlpatterns = [
    path('', views.storepage, name='storepage'),
    path('subscribe/', views.subscribe, name='subscribe'),
    path('get_stores/', views.store_service, name='store_service'),
]