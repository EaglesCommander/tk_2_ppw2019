from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt

from .forms import Newsletter
from .models import Store, Newsletter_Member
from search_result.models import Electronic
from random import sample
from search_result.models import Electronic

def storepage(request):
    
    if (request.method == 'GET'):
        response = {'user':request.user, 'method':'get'}
        return render(request, "storepage.html", response)

    try:
        if (request.method == 'POST'):
            item_id = request.POST.get("item_id")
            item_name = Electronic.objects.get(item_id=item_id).name
    except:
        item_id = 1
        item_name = "PLACEHOLDER"

    response = {"item_name":item_name, "item_id":item_id, "newsletter":Newsletter, 'user':request.user, 'method':'post'}

    return render(request, "storepage.html", response)

def store_service(request):

    stores = list(Store.objects.values())
    
    return JsonResponse(stores, safe=False)

@csrf_exempt
def subscribe(request):
    if(request.method == 'POST'):
        email = request.POST.get('email', None)

        member = Newsletter_Member(email=email)
        member.save()
    
    return HttpResponseRedirect('/storepage/')


