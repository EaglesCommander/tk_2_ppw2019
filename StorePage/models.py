from django.db import models
from django.utils import timezone
from datetime import datetime, date

class Newsletter_Member(models.Model):
    email = models.EmailField(("Email"), max_length=254)
    class Meta:
        db_table = u'email_list'

class Store(models.Model):
    store_id = models.IntegerField(("id"))
    picture = models.ImageField(("Image"), upload_to="store_img", height_field=None, width_field=None, max_length=None, null=True)
    name = models.CharField(("Name"), max_length=50)
    address_first = models.CharField(("Adress_first"), max_length=50)
    address_second = models.CharField(("Address_second"), max_length=50)
    details = models.CharField(("Details"), max_length=500)
    class Meta:
        db_table = u'store_list'