from django.urls import path

from . import views

app_name = "market"

urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('booking/', views.booking, name='booking'),
    path('booking/add-booking/',views.AddBooking, name='add booking'),
    path('subscribe/', views.subscribe_from_homepage, name='subscribe'),
    path('get_userbase/', views.get_userbase, name='get_userbase'),
]
