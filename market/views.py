from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from .forms import Search_image, BookingForm
from .models import Booking
from random import sample
from django.contrib.auth import login
from django.contrib.auth.forms import AuthenticationForm
from StorePage.forms import Newsletter
from StorePage.models import Newsletter_Member
from django.views.decorators.csrf import csrf_exempt

def homepage(request):
	form = AuthenticationForm()
	response = {
		"Search_image":Search_image,
		'form': form,
		 "newsletter":Newsletter,
		 'user':request.user
	}
	return render(request, "Homepage.html", response)

@csrf_exempt
def booking(request):
	#TODO: get objectID from request.POST
	response = {"Search_image":Search_image, 'Booking':BookingForm}
	return render(request, "Booking.html", response)

@csrf_exempt
def AddBooking(request):
	form = BookingForm(request.POST or None)
	
	if(request.method == 'POST' and form.is_valid()):
		name = form.cleaned_data['name']
		email = form.cleaned_data['email']
		phone_number = form.cleaned_data['phone_number']
		message = form.cleaned_data['message']

		booking = Booking(name=name, email=email, phone_number=phone_number, message=message)
		# booking.save()

	return HttpResponseRedirect('/')

def get_userbase(request):
	userbase = list(Newsletter_Member.objects.values())
	
	return JsonResponse(userbase, safe=False)


@csrf_exempt
def subscribe_from_homepage(request):
    if(request.method == 'POST'):
        email = request.POST.get('email', None)

        member = Newsletter_Member(email=email)
        member.save()
    
    return HttpResponseRedirect('/')



