from django.test import TestCase
from django.test import Client
from . import views
from django.urls import resolve
from django.http import HttpRequest
from . import models
from StorePage.models import Newsletter_Member

class StorePageUnitTest(TestCase):

    def test_homepage_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_subscribe_exist(self):
        response = Client().get('/subscribe/')
        self.assertEqual(response.status_code, 302)
        
    def test_using_subscribe_func(self):
        found = resolve('/subscribe/')
        self.assertEqual(found.func, views.subscribe_from_homepage)

    def test_store_exist(self):
        response = Client().get('/get_userbase/')
        self.assertEqual(response.status_code, 200)
    
    def test_using_store_func(self):
        found = resolve('/get_userbase/')
        self.assertEqual(found.func, views.get_userbase)

    def test_newsletter_form_works(self):
        client = Client()
        response = client.post("/subscribe/", {'email':'shella@gmail.com'})

        self.assertEqual(Newsletter_Member.objects.count(), 1)