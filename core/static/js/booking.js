$(document).ready(function(){
    get_userbase();
    getNews();
});

$(document).on("submit", "#sign-up", function(event){
    event.preventDefault();

    var query = $('#email-query').val();
    
    $.ajax({
        type:"POST",
        url:"subscribe/",
        data:{
            'email':query
        },
        success: function(){
            $('#sign-up').trigger("reset");
            $('#unauthed').empty();
            $('#unauthed').append("<p class='h4'>Email successfully added!</p>");
        }
    });
    
    return false;
});

function get_userbase(){

    $.ajax({
        type:"GET",
        url:"/get_userbase/",
        dataType: 'json',
        success: function(json){
            var userbase = json;

            $('#usernum').empty();
            $('#usernum').append('We have ' + json.length + ' users for our newsletter!');
        }
    });
    
    return false;
}

function getNews(){
    $.ajax({
        type:"GET",
        url:"https://newsapi.org/v2/top-headlines?country=id&apiKey=fab52125e09345d195306c90186217fa&category=technology",
        dataType: 'json',
        success: function(respon){
            var totalResult = respon.totalResults;

            var index = getRandInteger(0, Math.min(20, totalResult) - 1);

            var enter = "<br>"
            var title = "";
            if(respon.articles[index].title != null){
                var title = "<div class='h4'>" + respon.articles[index].title + "</div>";
            }
            var img = "<img src=\"" + respon.articles[index].urlToImage + "\" alt=\"failed\" class=\"news-image\">";
            var date = "<h5>Dipublikasikan pada : " + formatTime(new Date(respon.articles[index].publishedAt)) + "</h5>";
            
            var readMore = "<a href=\" " + respon.articles[index].url + " \"  width=\"50%\">" + "Baca Selengkapnya disini</a>"
            
            $('#news').append(title+enter+img+date+readMore);
        }
    });
    
    return false;
}

function getRandInteger(min, max){
    return Math.floor(Math.random() * (max - min + 1) ) + min;
}

function formatTime(time){
    var monthNames = [
        "Januari", "Februari", "Maret",
        "April", "Mei", "Juni", "Juli",
        "Agustus", "September", "Oktober",
        "November", "Desember"
    ];

    var day = time.getDate();
    var monthIndex = time.getMonth();
    var year = time.getFullYear();
    var hours = time.getHours();
    var minute = time.getMinutes();
    var second = time.getSeconds();

    if(hours < 10){
        hours = "0" + hours;
    }

    if(minute < 10){
        minute = "0" + minute;
    }

    if(second < 10){
        second = "0" + second;
    }

    return day + ' ' + monthNames[monthIndex] + ' ' + year + ' pukul: ' +  hours + ":" + minute + ":" + second;
}