$(document).ready(function(){
    get_result();
});

$(document).on("submit", "#sign-up", function(event){
    event.preventDefault();

    var query = $('#email-query').val();
    
    $.ajax({
        type:"POST",
        url:"subscribe/",
        data:{
            'email':query
        },
        success: function(){
            $('#sign-up').trigger("reset");
            $('#unauthed').empty();
            $('#unauthed').append("<p class='h4'>Email successfully added!</p>");
        }
    });
    
    return false;
});

function get_result(){

    $.ajax({
        type:"GET",
        url:"get_item/",
        dataType: 'json',
        success: function(json){
            var items = json;
            var random_num = Math.round(Math.random() * items.length);
            var brand = items[random_num].brand;
            var picture = "http://" + window.location.host + '/store_img/' + items[random_num].picture;
            var name = items[random_num].name;
            var processor = items[random_num].processor;
            var display = items[random_num].display;
            var graphic = items[random_num].graphic;
            var memory = items[random_num].memory;
            var camera = items[random_num].camera;
            var battery = items[random_num].battery;

            var push_brand =
            '<img class="store_img" src="' + picture + '"><br>' +
            '<h2><strong>' +  name + '</strong></h2>'
            var push_desc =
            '<h4>Brand</h4>'+
            '<h6>' + brand + '</h6>' +
            '<h4>Processor</h4>' +
            '<h6>' + processor + '</h6>' +
            '<h4>Display</h4>' +
            '<h6>' + display + '</h6>' +
            '<h4>Graphic</h4>' +
            '<h6>' + graphic + '</h6>' +
            '<h4>Memory</h4>' +
            '<h6>' + memory+ '</h6>' +
            '<h4>Camera</h4>' +
            '<h6>' + camera + '</h6>' +
            '<h4>Battery</h4>' +
            '<h6>' + battery + '</h6>'

            $('#result').empty();
            $('#desc').empty();
            $('#result').append(push_brand);
            
            $('#desc').append(push_desc);
        }
    });
    
    return false;
}