$(document).ready(function(){
    get_userbase();
});

$("#usernum").click(function(event){
    event.preventDefault();
    get_userbase();
});

$(document).on("submit", "#sign-up", function(event){
    event.preventDefault();

    var query = $('#email-query').val();
    
    $.ajax({
        type:"POST",
        url:"subscribe/",
        data:{
            'email':query
        },
        success: function(){
            $('#sign-up').trigger("reset");
            $('#unauthed').empty();
            $('#unauthed').append("<p class='h4'>Email successfully added!</p>");
        }
    });
    
    return false;
});

function get_userbase(){

    $.ajax({
        type:"GET",
        url:"get_userbase/",
        dataType: 'json',
        success: function(json){
            var userbase = json;

            $('#usernum').empty();
            $('#usernum').append('We have ' + json.length + ' users for our newsletter!');
        }
    });
    
    return false;
}