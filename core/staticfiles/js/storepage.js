$(document).ready(function(){
    get_stores();
});

$("#refresh").click(function(event){
    event.preventDefault();
    get_stores();
});

$(document).keypress(function(event){
    if (event.which == '114' && document.activeElement.type != 'email'){
        get_stores();
    }
});

$(document).on("submit", "#sign-up", function(event){
    event.preventDefault();

    var query = $('#email-query').val();
    
    $.ajax({
        type:"POST",
        url:"subscribe/",
        data:{
            'email':query
        },
        success: function(){
            $('#sign-up').trigger("reset");
            $('#unauthed').empty();
            $('#unauthed').append("<p class='h4'>Email successfully added!</p>");
        }
    });
    
    return false;
});

function get_stores(){

    $.ajax({
        type:"GET",
        url:"/storepage/get_stores/",
        dataType: 'json',
        success: function(json){
            $("#store-result").empty();
            var stores = json
            var number_collection = [];
            var random_num = Math.round(Math.random() * stores.length);
            var csrftoken = Cookies.get('csrftoken');

            for(i=0; i < 3; i++) {
                while ((number_collection.includes(random_num)) || random_num == stores.length){
                    var random_num = Math.round(Math.random() * stores.length);
                }
                
                number_collection.push(random_num);

                var id = stores[random_num].store_id;
                var name = stores[random_num].name;
                var address = stores[random_num].address_first;
                var detail = stores[random_num].details;
                var pic = "http://" + window.location.host + '/store_img/' + stores[random_num].picture;

                if (detail.length > 50){
                    detail = detail.substring(0,47);
                    detail = detail.concat("...");
                }

                if ($('#authed').length == 0){
                    var item = '<div class="row">' +
                    '<div class="col">' +
                    '<div class="row">' +
                    '<img class="store_img" src="' + pic + '">' + 
                    '</div>' + 
                    '</div>' +
                    '<div class="col">' + 
                    '<div class="row">' + 
                    '<p class="storepage_text h4">Nama : ' + name + '</p></div>' +
                    '<div class="row">' + 
                    '<p class="storepage_text h4">Alamat : ' + address + '</p></div>' +
                    '<div class="row">' + 
                    '<p class="storepage_text h4">Detail : ' + detail + '</p></div>' +
                    '<div class="row">' +
                    '<form action="/login/" method="GET">' +
                    '<input type="submit" value="Login to book">' +
                    '</form>' +
                    '</div>' +
                    '<br><br>' +
                    '</div>'
                }
                else {
                    var item = '<div class="row">' +
                    '<div class="col">' +
                    '<div class="row">' +
                    '<img class="store_img" src="' + pic + '"></div></div>' +
                    '<div class="col">' + 
                    '<div class="row">' + 
                    '<p class="storepage_text h4">Nama : ' + name + '</p></div>' +
                    '<div class="row">' + 
                    '<p class="storepage_text h4">Alamat : ' + address + '</p></div>' +
                    '<div class="row">' + 
                    '<p class="storepage_text h4">Detail : ' + detail + '</p></div>' +
                    '<div class="row">' +
                    '<form action="/booking/" method="POST">' +
                    '<input type="hidden" name="csrfmiddlewaretoken" value="' + csrftoken +  '"></input>' +
                    '<input type="hidden" name="store_id" value=' + id + '>' +
                    '<input type="hidden" name="item_id" value="1">' +
                    '<input type="submit" value="Book">' +
                    '</form>' +
                    '</div>' +
                    '<br><br>' +
                    '</div>'
                    
                }

                $("#store-result").append(item);
            }
        }
    });
    
    return false;
}